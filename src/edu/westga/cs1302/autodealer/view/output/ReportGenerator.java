package edu.westga.cs1302.autodealer.view.output;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

/**
 * The Class ReportGenerator.
 * 
 * @author CS1302
 */
public class ReportGenerator {

	/**
	 * Builds the full summary report of the specified inventory. If inventory is
	 * null, instead of throwing an exception will return a string saying "No
	 * inventory exists.", otherwise builds a summary report of the inventory.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param inventory the inventory
	 *
	 * @return A formatted summary string of the dealership's automobile inventory.
	 */
	@SuppressWarnings("unused")
	public String buildFullSummaryReport(Inventory inventory) {
		String summary = "";
		String highestSummary = "";
		String lowestSummary = "";
		double min = 0;
		double max = 999999999;
		
		for (Automobile current: inventory.getAutos()) {
			if (current.getPrice() > min) {
				min = current.getPrice();
				highestSummary = ("Most Expensive:\nMake:" + current.getMake() + "\nModel: " + current.getModel() + "\nYear: " + current.getYear() + "\nPrice: " + current.getPrice());
			}
		}
		for (Automobile current: inventory.getAutos()) {
			if (current.getPrice() < max) {
				max = current.getPrice();
				lowestSummary = ("Cheapest:\nMake:" + current.getMake() + "\nModel: " + current.getModel() + "\nYear: " + current.getYear() + "\nPrice: " + current.getPrice());
			}
		}
		if (inventory == null) {
			summary = "No inventory to build report for.";
			
		} else {
			summary = (inventory.getDealershipName() + "\n#Automobiles: " + inventory.size() + "\n\n" + highestSummary + "\n\n" + lowestSummary);
		}

		return summary;
	}

}
