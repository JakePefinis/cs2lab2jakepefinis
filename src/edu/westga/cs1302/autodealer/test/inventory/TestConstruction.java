package edu.westga.cs1302.autodealer.test.inventory;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Inventory;

class TestConstruction {
	@Test
	void testDefaultInventory() {
		Inventory inventory = new Inventory();

		assertAll(() -> assertEquals(0, inventory.size()), () -> assertNull(inventory.getDealershipName()));
	}

	@Test
	void testInventoryNullName() {
		assertThrows(IllegalArgumentException.class, () -> new Inventory(null));
	}
	
	@Test
	void testInventoryEmptyName() {
		assertThrows(IllegalArgumentException.class, () -> new Inventory(""));
	}
	
	@Test
	void testValidName() {
		Inventory inventory = new Inventory("Test");
		
		assertAll(() -> assertEquals(0, inventory.size()), () -> assertEquals("Test", inventory.getDealershipName()));
	}
	
	

}
